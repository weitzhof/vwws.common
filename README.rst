===========
vwws.common
===========

Verwaltungs-Webservices HAW Landshut: Gemeinsam genutzte Komponenten


Installation
============

Vorausgesetzt werden ein aktuelles Betriebssystem (vorzugsweise Linux) sowie
folgende Software:


- Python_ 3.3 oder neuer

- Eine Umgebung zum Kompilieren von in C geschriebenen Python-3-Bibliotheken

- Git_ zum Herunterladen (Klonen) von Git-Repositories

- virtualenv_ zum Erstellen isolierter Python-Umgebungen

- pip_ zum Installieren von Python-Paketen


Unter Debian_ (ab Version 8) bzw. Ubuntu_ (Ab Version 13.04) reichen dafür
folgende Kommandos::

 $ sudo apt-get install python3 python3-dev build-essential
 $ sudo apt-get install git python-virtualenv python-pip


virtualenv einrichten
---------------------

Falls noch keine virtuelle Python-Umgebung (*virtualenv*) für
Verwaltungs-Webservices besteht, kann diese folgendermaßen angelegt werden::

 $ virtualenv -p /usr/bin/python3 /pfad/zu/vwws-env

virtualenv aktivieren::

 $ source /pfad/zu/vwws-env/bin/activate

Im Folgenden wird davon ausgegangen, dass immer mit aktiviertem virtualenv
gearbeitet wird.


vwws.common installieren
------------------------

vwws.common installieren::

 $ pip install git+https://bitbucket.org/weitzhof/vwws.common

**Hinweis:** Alle Komponenten von vwws können im selben virtualenv installiert
werden. Falls vwws.common dort bereits vorliegt, muss dieses Paket natürlich
nicht neu installiert werden.


Test
====

Falls alles geklappt hat, sollte man (bei aktiviertem Virtualenv) in Python 3
das Modul ``vwws.common`` ohne Fehlermeldung importieren können::

 $ python3
 >>> import vwws.common
 >>> vwws.common.__version__
 '0.1.0'

Die Versionsnummer kann hierbei natürlich abweichen.


.. _Git: http://git-scm.com
.. _pip: https://pip.pypa.io
.. _Python: https://www.python.org
.. _virtualenv: https://virtualenv.readthedocs.org

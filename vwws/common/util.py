'''Allgemeine Utilities'''

# stdlib
from contextlib import contextmanager
from datetime import date, time, datetime
from functools import wraps
from json import JSONDecoder, JSONEncoder
import logging
import re
import sys
from time import time as current_time

# exportierte Namen
__all__ = [
    'isotime_regex',
    'is_isotime',
    'isotime_to_time',
    'rfc3339_regex',
    'is_rfc3339',
    'rfc3339_to_datetime',
    'ExtendedJSONEncoder',
    'extended_json_object_hook',
    'time_this'
]


log = logging.getLogger(__name__)


# vgl. https://docs.python.org/3/library/datetime.html#time-objects
isotime_regex = re.compile(
    r'''^
        \d\d:\d\d:\d\d                  # Stunde:Minute:Sekunde
        (?P<frac> \.\d+)?               # optional: Sekundenbruchteile
        $
    ''', flags=re.VERBOSE
)


def is_isotime(s):
    '''True liefern wenn s das Format von datetime.time.isoformat() hat

    Timezone-Aware-Zeiten werden dabei absichtlich NICHT unterstützt

    '''
    return bool(rfc_regex.match(s))


def isotime_to_time(s):
    '''String wie datetime.time.isoformat() in datetime.time konvertieren'''
    # String zerlegen, Exception werfen falls Format nicht passt
    match = isotime_regex.match(s)
    if not match:
        raise ValueError('"{}" does not conform to '
                         'datetime.time.isotime()-format'.format(s))
    dct = match.groupdict()
    # Format bestimmen (je nachdem ob Sekundenbruchteile angegeben) und parsen
    fmt = '%H:%M:%S.%f' if dct['frac'] else '%H:%M:%S'
    return datetime.strptime(s, fmt).time()


# vgl. tools.ietf.org/html/rfc3339
rfc3339_regex = re.compile(
    r'''^
        (?P<partial>
            \d\d\d\d-\d\d-\d\d              # Jahr-Monat-Tag
            T\d\d:\d\d:\d\d                 # Stunde:Minute:Sekunde
            (?P<frac> \.\d+)?               # optional: Sekundenbruchteile
        )
        (
            (?P<is_utc> Z) |                # entweder: UTC
            (                               # oder: offset zu UTC
                (?P<offset1> [+-]\d\d) :
                (?P<offset2> \d\d)
            )
        )
        $
    ''', flags=re.VERBOSE | re.IGNORECASE
)


def is_rfc3339(s):
    '''True liefern wenn s eine RFC3339-konforme datetime-Repräsentation ist'''
    return bool(rfc_regex.match(s))


def rfc3339_to_datetime(s):
    '''RFC3339-String in datetime konvertieren'''
    # String zerlegen, Exception werfen falls String nicht RFC3339-konform
    match = rfc3339_regex.match(s)
    if not match:
        raise ValueError('"{}" does not conform to RFC3339'.format(s))
    dct = match.groupdict()
    # UTC-Offset vereinheitlichen
    tpl = '{partial}+0000' if  dct['is_utc'] else '{partial}{offset1}{offset2}'
    s_normalized = tpl.format(**dct)
    # Format bestimmen (je nachdem ob Sekundenbruchteile angegeben) und parsen
    fmt = '%Y-%m-%dT%H:%M:%S.%f%z' if dct['frac'] else '%Y-%m-%dT%H:%M:%S%z'
    return datetime.strptime(s_normalized, fmt)


class ExtendedJSONEncoder(JSONEncoder):
    '''JSONEncoder mit erweiterter Funktionalität'''

    def default(self, obj):
        if isinstance(obj, (date, time, datetime)):

            # Zeitbezogene Objekte normalisieren und mit Type-Hint versehen
            return{
                '__type__': obj.__class__.__name__,
                'isoformat': obj.isoformat()
            }

        # Für alles andere: Superklasse
        return super().default(obj)


def extended_json_object_hook(dct):
    '''Object Hook für JSON-Decoder mit erweiterter Funktionalität'''
    if '__type__' in dct:
        type_ = dct['__type__']
        if type_ == 'date':
            return datetime.strptime(dct['isoformat'], '%Y-%m-%d').date()
        elif type_ == 'time':
            return isotime_to_time(dct['isoformat'])
        elif type_ == 'datetime':
            return rfc3339_to_datetime(dct['isoformat'])
    return dct


@contextmanager
def time_this(name=None, logger=None, level=logging.DEBUG):
    '''Liefere Kontextmanager/Dekorator, der Ausführungsdauer loggt

    ... und zwar in den angegebenen Logger mit dem angegebenen Level. Falls
    kein Logger angegeben wird, wird direkt nach stderr geschrieben.

    '''
    t0 = current_time()
    yield
    dt = current_time() - t0
    msg = '{} took {:.4f} seconds to execute'.format(name, dt)
    if logger:
        logger.log(level, msg)
    else:
        print(msg, file=sys.stderr)

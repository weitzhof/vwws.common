'''Zusatzfunktionalität für morepath-Views'''

# stdlib
import json

# 3rd party
from morepath.request import Response

# exportierte Namen
__all__ = [
    'dictify',
    'dictify_collection',
    'get_render_json',
    'to_unauthorized',
]


def dictify(self, *, links=None):
    '''Ein Objekt für JSON-Ausgabe als dict repräsentieren ...

    ... und dieses dict um den Typ der Resource und optional um Links
    erweitern.

    Die Ressource muss die Methode asdict() zur Verfügung stellen.

    '''
    result = self.asdict()
    result['__type__'] = self.__class__.__name__
    if links is not None:
        result['__links__'] = links
    return result


def dictify_collection(self, *, links=None, items, linkmaker=None):
    '''Eine Sammlung von Objekten für JSON-Ausgabe als dict repräsentieren ...

    ... und dieses dict um den Typ der Resource, Repräsentationen der
    enthaltenen Ressourcen (items) und optional um Links erweitern.

    Die Ressource und alle enthaltenen Ressourcen (items) müssen die Methode
    asdict() zur Verfügung stellen.

    Wird ein callable "linkmaker" angegeben, wird für jede enthaltene Ressource
    ein Links-Dict mit einem self-Link hinzugefügt.

    '''
    if linkmaker:
        item_dicts = [dictify(i, links={'self': linkmaker(i)}) for i in items]
    else:
        item_dicts = [dictify(i) for i in items]
    result = dictify(self, links=links)
    result['__items__'] = item_dicts
    return result


def get_render_json(skipkeys=False,
                    ensure_ascii=True,
                    check_circular=True,
                    allow_nan=True,
                    cls=None,
                    indent=None,
                    separators=None,
                    default=None,
                    sort_keys=False,
                    **kwargs):
    '''Angepasste Funktion zum Rendern von JSON-Responses zurückliefern

    Argumente und ihre Default-Werte sind ident mit denen von json.dump (bis
    auf den fehlenden content, der das einzige Argument der erzeugten Funktion
    ist)

    '''
    def render_json(content):
        '''Angepasste Funktion zum Rendern einer JSON-Response'''
        return Response(
            json.dumps(
                content,
                skipkeys,
                ensure_ascii,
                check_circular,
                allow_nan,
                cls,
                indent,
                separators,
                default,
                sort_keys,
                **kwargs
            ),
            content_type='application/json'
        )

    return render_json

def to_unauthorized(self, request):
    '''Response auf beliebigen Request in HTTP 401 (Unauthorized) umwandeln

    Um die Abfrage von Benutzername und Passwort für HTTP Basich Auth
    auszulösen, muss Statuscode 401 sein und der Header 'WWW-Authenticate'
    gesetzt sein.

    '''
    @request.after
    def set_status_code(response):
        response.status_code = 401
        response.headers['WWW-Authenticate'] = 'Basic realm="%s"' % __package__
    return 'HTTP 401: Unauthorized'

'''Authentifizierungs-Modul'''

# stdlib
from collections import defaultdict
import logging

# 3rd party
from passlib.apache import htpasswd_context, HtpasswdFile

# exportierte Namen
__all__ = ['User', 'get_users', 'is_valid_identity']


log = logging.getLogger(__name__)


class User:
    '''Eine Klasse zum Repräsentieren von Benutzern

    Ein User-Objekt enthält ein set von Gruppennamen, denen der Benutzer
    angehört.

    '''
    def __init__(self, name, pwd_hash, groups=None):
        self.name = name
        self._pwd_hash = pwd_hash
        self.groups = groups if groups is not None else set()

    def check_password(self, password):
        return htpasswd_context.verify(
            password.encode('utf-8'),
            self._pwd_hash
        )

    @property
    def is_admin(self):
        return 'admin' in self.groups

    def in_group(self, group):
        return group in self.groups

    def __str__(self):
        return 'user:{}'.format(self.name)

    def __repr__(self):
        return '{}({!r}, ..., {!r})'.format(
            self.__class__.__name__,
            self.name,
            self.groups
        )


def get_users(user_file, group_file=None):
    '''Ein Mapping von Usernamen auf User-Objekten zurückliefern'''

    # Gruppendatei lesen
    groups = defaultdict(lambda: set())
    if group_file:
        log.debug('Lade Gruppendatei {}'.format(group_file))
        with open(group_file, encoding='utf-8') as f:
            for line in f:

                # Leerzeilen und Kommentare ignorieren
                if line.isspace() or line.lstrip().startswith('#'):
                    continue

                # Gruppe und zugeordnete Benutzer ermitteln
                g, u = line.split(':', 1)
                group = g.strip()
                for username in u.split():
                    groups[username].add(group)

    # User/Passwort-Datei lesen
    log.debug('Lade Benutzerdatei {}'.format(user_file))
    ht = HtpasswdFile(user_file)

    # Und Benutzerobjekte erzeugen/liefern
    users = {}
    for username in ht.users():
        user = User(username, ht.get_hash(username), groups[username])
        users[username] = user
    return users


def is_valid_identity(identity, valid_users):
    '''True liefern, wenn Identität zu Benutzer in valid_users passt'''
    if not identity.userid:
        return False
    if identity.userid not in valid_users:
        return False
    if valid_users[identity.userid].check_password(identity.password):
        return True
    return False

import unittest

from .. import model


class FooModel(model.FlexModel):
    _required_fields = ('req1', 'req2')
    _fields = ('req1', 'req2', 'opt1', 'opt2')


class TestModel(unittest.TestCase):

    def test_required(self):
        # Nur eines von zwei benötigten Feldern angeben
        with self.assertRaises(ValueError):
            foo = FooModel(req1=1)

    def test_sparse(self):
        # Nur eines von zwei optionalen Feldern angeben
        data = {'req1': 1, 'req2': 2, 'opt1': 3}
        foo = FooModel(**data)
        self.assertTrue(foo.is_sparse)
        self.assertFalse(foo.is_bloated)
        self.assertEqual(foo.asdict(), data)
        self.assertEqual(set(foo.defined_fields), set(data))
        self.assertEqual(list(foo.excess_fields), [])
        self.assertEqual(foo.asdict(), data)

    def test_full(self):
        # Nur eines von zwei optionalen Feldern angeben
        data = {'req1': 1, 'req2': 2, 'opt1': 3, 'opt2': 4}
        foo = FooModel(**data)
        self.assertFalse(foo.is_sparse)
        self.assertFalse(foo.is_bloated)
        self.assertEqual(foo.asdict(), data)
        self.assertEqual(set(foo.defined_fields), foo.fieldset)
        self.assertEqual(list(foo.excess_fields), [])
        self.assertEqual(foo.asdict(), data)

    def test_bloated(self):
        # ein zusätzliches feld angegeben
        data = {'req1': 1, 'req2': 2, 'opt1': 3, 'exc1': 4}
        foo = FooModel(**data)
        self.assertTrue(foo.is_sparse)
        self.assertTrue(foo.is_bloated)
        self.assertEqual(foo.asdict(), data)
        self.assertEqual(set(foo.excess_fields), set(['exc1']))

    def test_model_inheritance(self):
        class M1(model.FlexModel):
            _fields = ['a', 'b']
            _required_fields = ['a', 'b']

        class M2(model.FlexModel):
            _fields = ['c']

        class M3(M1, M2):
            _fields = ['d']
            _required_fields = ['c']

        self.assertEqual(M3._fieldset, set(['a', 'b', 'c', 'd']))
        self.assertEqual(M3._required_fieldset, set(['a', 'b', 'c']))

if __name__ == '__main__':
    unittest.main()

import unittest

from .. import model, view


class FooModel(model.FlexModel):
    _required_fields = ('req1', 'req2')
    _fields = ('req1', 'req2', 'opt1', 'opt2')


class BarCollection(model.Collection):
    pass


class TestView(unittest.TestCase):

    def test_dictify_flex_model(self):
        datalist = [
            {'req1': 1, 'req2': 2, 'opt1': 3},
            {'req1': 1, 'req2': 2, 'opt1': 3, 'opt2': 4},
        ]
        links = {'foo': 'bar'}
        for data in datalist:
            foo = FooModel(**data)
            result = view.dictify(
                foo,
                links=links,
            )
            expected = dict(
                data,
                __links__=links,
                __type__='FooModel'
            )
            self.assertEqual(result, expected)

    def test_dictify_collection(self):
        data = {'req1': 1, 'req2': 2, 'opt1': 3}
        links = {'foo': 'bar'}
        linkmaker = lambda x: 'some_link_to_item'
        foo = FooModel(**data)
        coll = BarCollection()
        result = view.dictify_collection(
            coll,
            links=links,
            items=[foo],
            linkmaker=linkmaker
        )
        expected = dict(
            {},
            __links__=links,
            __type__='BarCollection',
            __items__=[
                view.dictify(foo, links={'self': 'some_link_to_item'})
            ]
        )
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

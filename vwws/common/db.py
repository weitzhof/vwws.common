'''Zusatzfunktionalität für SQLAlchemy'''

# stdlib
import logging

# 3rd party
import sqlalchemy

# exportierte Namen
__all__ = ['bless_url', 'tested_engine', 'EngineRouter', 'router']


log = logging.getLogger(__name__)


def bless_url(name_or_url, **kwargs):
    '''name_or_url in URL-Objekt umwandeln, Attribute dazu und zurückliefern

    Ist name_or_url bereits vom Typ sqlalchemy.engine.url.URL, wird auf diesem
    Objekt direkt gearbeitet

    '''
    url = sqlalchemy.engine.url.make_url(name_or_url)
    for key, value in kwargs.items():
        setattr(url, key, value)
    return url


def tested_engine(engine):
    '''DB-Engine testen und wieder zurückliefern (oder Exception werfen).'''
    try:
        engine.execute('SELECT 1')
    except sqlalchemy.exc.DBAPIError as e:

        # zu langen Stacktrace unterbinden: falls eine orig-Exception
        # vorliegt, nur diese werfen (ohne Kontext)
        if e.orig:
            raise e.orig from None

        # sonst exception unverändert werfen
        raise

    return engine


# TODO: Für EngineRouter bietet sich das "reg"-Framework an, das auch von
# morepath eingesetzt wird
class EngineRouter:
    '''Ein EngineRouter liefert für eine Model-Klasse eine passende DB-Engine

    (vorausgesetzt, dass diese zuvor per "register_engine" registriert wurde)

    '''
    def __init__(self):
        self._engines = {}
        self._cache = {}


    def register_engine(self, engine, for_cls=object):
        '''DB-Engine für eine bestimmte Model-Klasse registrieren

        Klasse 'object' kann als Fallback für beliebige Modell-Klassen genutzt
        werden - auf diese Weise lässt sich eine Default-Engine für beliebige
        Modelle definieren.

        '''
        self._engines[for_cls] = engine
        self._cache.clear()

    def get_engine(self, for_cls=object):
        '''DB-Engine für eine bestimmte Model-Klasse zurückliefern

        (oder DB-Engine für die nächste Superklasse in der MRO zurückliefern,
        für die eine DB-Engine ermittelt werden kann). ValueError, falls nichts
        gefunden.

        '''
        if for_cls not in self._cache:
            for c in for_cls.mro():
                if c in self._engines:
                    self._cache[for_cls] = self._engines[c]
                    break
        try:
            return self._cache[for_cls]
        except KeyError:
            raise ValueError('no database engine found for {}'.format(for_cls))


router = EngineRouter()

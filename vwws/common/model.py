'''Zusatzfunktionalität für morepath-Modelle'''

# stdlib
from abc import ABCMeta
from collections.abc import MutableMapping
from keyword import iskeyword
from textwrap import dedent

# projekt
from . import db

# exportierte Namen
__all__ = ['FlexModel', 'Collection', 'WithEngine', 'AppRoot']


class FlexModelMeta(ABCMeta):
    '''Metaklasse von FlexModel

    Diese Metaklasse stellt sicher, dass Subklassen von FlexModel bestimmte
    Attribute haben ("_fieldset" und "_required_fieldset")

    Sie ist Subklasse von ABCMeta, damit FlexModel-Klassen von
    collections.abc.MutableMapping erben können (sonst gäbe es einen metaclass
    conflict)

    '''
    def __new__(mcls, name, bases, dct):

        # Ergänzen des Klassen-Dicts nur für Subklassen von FlexModel (da wo
        # also eine Instanz von FlexModelMeta selbst in den Basisklassen ist),
        # nicht für FlexModel-Klasse selbst
        ancestors = [b for b in bases if isinstance(b, FlexModelMeta)]
        if not ancestors:
            return super().__new__(mcls, name, bases, dct)

        # Feldspezifikationen vorbereiten.
        fieldset = set(dct.get('_fields', []))
        for a in ancestors:
            if hasattr(a, '_fields'):
                fieldset |= set(a._fields)
        required_fieldset = set(dct.get('_required_fields', []))
        for a in ancestors:
            if hasattr(a, '_required_fields'):
                required_fieldset |= set(a._required_fields)

        # Feldspezifikationen validieren
        # TODO: eventuell strikter sein
        if not required_fieldset <= fieldset:
            raise ValueError('required fields must be a subset of fields')

        # Klassen-Namensraum erweitern
        dct['_fieldset'] = fieldset
        dct['_required_fieldset'] = required_fieldset

        return super().__new__(mcls, name, bases, dct)


class FlexModel(MutableMapping, metaclass=FlexModelMeta):
    '''Flexibles Modell (Felder nur teilweise vorgegeben)

    Nicht immer muss ein Modellobjekt komplett mit Daten befüllt werden: Soll
    zum Beispiel auf Modellobjekte verlinkt werden, reicht es aus, diese
    lediglich mit dem Objektidentifikator zu befüllen.

    FlexModel trägt dem Rechnung, indem bei der Objektinitialisierung nur
    bestimmte Felder Pflicht sind (definiert über Klassenattribut
    _required_fields).

    Vorgesehene Felder werden über das Klassenattribut _fields spezifiziert
    (muss eine Übermenge von _required_fields sein).

    Bei der Instanzierung, über die Methode extend() und per __setitem__ können
    auch nicht vorgesehene Felder angegeben werden. So kann einem Modellobjekt
    zusätzliche "Nutzlast" mitgegeben werden ("excess fields"). Ob dies eine
    gute Idee ist, sei vorerst dahingestellt und wird sich in der Praxis zeigen
    müssen.

    '''
    def __init__(self, **kwargs):

        # Alle Pflichtfelder vorhanden?
        for name in self._required_fieldset:
            if name not in kwargs:
                raise ValueError('Missing field "{}"'.format(name))
        self._data = kwargs

    @property
    def is_bloated(self):
        '''True, wenn mindestens ein nicht vorgesehenes Feld besetzt ist'''
        return any(field not in self._fieldset for field in self._data)

    @property
    def defined_fields(self):
        return self._data.keys()

    @property
    def excess_fields(self):
        '''Nicht vorgesehene aber trotzdem definierte Felder'''
        return set(self._data) - self._fieldset

    @property
    def fieldset(self):
        return self._fieldset

    @property
    def required_fieldset(self):
        return self._required_fieldset

    @property
    def is_sparse(self):
        '''True, wenn nicht alle vorgesehenen Felder besetzt sind'''
        return any(field not in self._data for field in self._fieldset)

    def __delitem__(self, key):
        '''Feld key löschen (falls erlaubt)'''
        if key in self._required_fieldset:
            raise KeyError(key)
        del self._data[key]

    def __getattr__(self, name):
        '''Direkter zugriff auf Felder aus self._data'''
        try:
            return self._data[name]
        except KeyError:
            raise AttributeError(name)

    def __getitem__(self, key):
        '''Zugriff auf Felder aus self._data'''
        try:
            return self._data[key]
        except KeyError:
            raise KeyError(key)

    def __iter__(self):
        '''Iterator über die Namen aller belegten Felder'''
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __repr__(self):
        return '{}(**{!r})'.format(self.__class__.__name__, self._data)

    def __setattr__(self, name, value):
        '''Direktes Setzen von Feldern in self._data falls name in _fieldset'''
        if name in self._fieldset:
            self._data[key] = value
        else:
            super().__setattr__(name, value)

    def __setitem__(self, key, value):
        '''Direkter Zugriff auf Felder aus self._data'''
        self._data[key] = value

    def asdict(self):
        '''dict-Repräsentation zurückliefern

        sinnvoll für JSON-Serialisierung

        '''
        return dict(self._data)


class Collection(FlexModel):
    '''Sammlung von Ressourcen'''


class WithEngine:
    '''Mixin zum komfortablen Zugriff auf DB-Engine für aktuelles Modell

    Geht davon aus, dass eine DB-Engine für Modell-Klasse bei db.router
    registriert ist.

    '''
    @classmethod
    def get_engine(cls):
        '''DB-Engine für aktuelles Modell zurückliefern'''
        return db.router.get_engine(for_cls=cls)


class AppRoot(FlexModel):
    '''Root einer app'''

    _fields = ['name', 'version', 'description']
    _required_fields = ['name', 'version']

    @classmethod
    def from_package(cls, package):
        '''Ein ServiceInformation-Objekt für ein Package zurückliefern'''
        version = getattr(package, '__version__', None)
        desc = package.__doc__.splitlines()[0] if package.__doc__ else None
        return cls(name=package.__name__, version=version, description=desc)

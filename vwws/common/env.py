'''Modul zum Auslesen von Umbgebungsvariablen in verschiedenen Formen'''

# stdlib
import ast
import os
from collections import defaultdict

LITERAL = 'LITERAL'


class EnvError(Exception):
    '''Basisklasse für alle umgebungsvariablenspezifischen Probleme'''


class EnvLookupError(LookupError, EnvError):
    '''Fehler bei Nicht gefundener Umgebungsvariable'''

    def __init__(self, name):
        self.name = name
        super().__init__(
            'environment variable "{}" could not be found'.format(name)
        )


class EnvValueError(ValueError, EnvError):
    '''Fehler beim Interpretieren des Wertes einer Umgebungsvariable'''

    def __init__(self, name, reason):
        self.name = name
        super().__init__(
            'environment variable "{}": {}'.format(name, reason)
        )


class Env:
    '''Klasse zum repräsentieren einer Umgebungsvariable'''

    def __init__(self, name):
        '''Wert einer Umgebungsvariable abfragen und als Env speichern'''
        self.name = name
        self.val = os.getenv(name)
        if self.val is None:
            raise EnvLookupError(name)

    def __repr__(self):
        return '{}({!r})'.format(
            self.__class__.__name__,
            self.name,
            self.val
        )

    def __str__(self):
        return self.val

    def __int__(self):
        try:
            return int(self.val)
        except ValueError as e:
            raise EnvValueError(self.name, str(e))

    def __float__(self):
        try:
            return float(self.val)
        except ValueError as e:
            raise EnvValueError(self.name, str(e))

    def __bool__(self):
        if self.val.lower() in ('true', 'yes', '1'):
            return True
        elif self.val.lower() in ('false', 'no', '0'):
            return False
        else:
            raise EnvValueError(
                self.name,
                'cannot interpret "{}" as boolean'.format(self.val)
            )

    def __iter__(self):
        return iter(self.val.split(','))

    def aslist(self, sep=','):
        '''Env-Var als Liste interpretieren und diese zurückliefern

        Dabei wird davon ausgegangen, dass die Elemente der Liste
        kommasepariert in der Umgebungsvariable stehen.

        '''
        return self.val.split(sep)

    def literal_eval(self):
        '''Env-Var als Python-Literal evaluieren und Ergebnis zurückliefern

        The value provided may only consist of the following Python literal
        structures: strings, numbers, tuples, lists, dicts, booleans, and None.

        '''
        try:
            return ast.literal_eval(self.val)
        except (SyntaxError, ValueError) as e:
            raise EnvValueError(self.name, str(e))
